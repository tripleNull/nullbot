# Nullbot: Simple bot to learn how to use the mastodon API
Code for nullbot: https://botsin.space/@nullbot

If using this script, set up a .env file to hold the CLIENT_KEY, CLIENT_SECRET and ACCESS_TOKEN generated for your application in your account through Mastodon. 
