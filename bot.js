// Code for @nullbot, hosted on botsin.space
// Followed example posted by Daniel Shiffman (The Coding Train) on YouTube (https://www.youtube.com/watch?v=4L4JyWyb3oI)
// http://codingtra.in

// Require the mastodon API and pull authentication variables from dotfiles (using the dotenv package).
const Mastodon = require('mastodon-api');
const ENV = require('dotenv');
ENV.config();

// Connect to the API 
const M = new Mastodon({
	client_key: process.env.CLIENT_KEY,
	client_secret: process.env.CLIENT_SECRET,
	access_token: process.env.ACCESS_TOKEN,
	timeout_ms: 60*1000,  // optional HTTP request timeout to apply to all requests.
	api_url: 'https://botsin.space/api/v1/', // optional, defaults to https://mastodon.social/api/v1/
})

// Open a stream listener
const listener = M.stream('streaming/hashtag?tag=kde')

listener.on('error', err => console.log(err))

// What to do when a message is received
listener.on('message', msg => {
    if (msg.event === 'update'){
    	if (msg.data){
    		const id = msg.data.id;
    		M.post(`statuses/${id}/reblog`, (error,data) => {
      			if (error) console.error(error);
      			else console.log(`Boosted ${id} from ${msg.data.account.acct}`);
    			})
    	}
    }
});

// Toot out content
function toot(content){

	const params = {
		status: content
	}

	M.post('statuses', params, (error, data) => {
		if (error){
			console.error(error);
		} else{
			console.log(data.content);	
		}
	})

}

// Send the new follower a random number
function followed(msg){
	const acct = msg.data.account.acct;
	const id = msg.data.account.id;
	const randnum = Math.floor(Math.random() * 100);
	toot(`@${acct} I'm just a simple nullbot, your random number is: ${randnum}`) 
}
